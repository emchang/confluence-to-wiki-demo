 #!/bin/bash

FILES=*.html
mkdir out/
for f in $FILES
do
  filename="${f%.*}"
  echo "Converting $f to $filename.md"
  `pandoc $f -f html -t markdown_strict -o out/$filename.md`
done
