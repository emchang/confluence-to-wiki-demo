## Introduction
This project is a demo on how to migrate a confluence space to a GitLab Project Wiki

## Steps
1. On Confluence, [Export space as HTML](https://confluence.atlassian.com/doc/export-content-to-word-pdf-html-and-xml-139475.html#ExportContenttoWord,PDF,HTMLandXML-ExportmultiplepagestoHTML,XML,orPDF).
1. Extract files into a folder. 
1. Parse HTML to Markdown. I used [pandoc](https://pandoc.org/)
    1. Install [pandoc](https://pandoc.org/installing.html)
    1. Run the `convert.sh` script inside the folder to convert all `.html` files to `.md` 
    1. The converted files are in `out/` of the folder
1.  [Clone GitLab Wiki Repo locally](https://docs.gitlab.com/ee/user/project/wiki/#adding-and-editing-wiki-pages-locally)
    - Example:  `git clone git@gitlab.com:emchang/confluence-to-wiki-demo.wiki.git`
1.  Move converted files `out/' to Wiki Repository
    - Example: `mv out/* confluence-to-wiki-demo.wiki/`
1.  Move `attachments` to Wiki Repository
    - Example: `mv attachments/ confluence-to-wiki-demo.wiki/`
1.  Add, commit and push the files to Wiki Repository
1.  Check wiki from GitLab UI, particularly pages with tables and images.

## Limitations
1. Page links to other wiki pages will point to broken html pages. 
1. Subpages will not be retained. 